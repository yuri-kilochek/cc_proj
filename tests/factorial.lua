function recursive_fact(n)
    if n == 0 then
        return 1
    else
        return n * recursive_fact(n - 1)
    end
end

function iterative_fact(n)
    local p = 1
    for i = 2, n do
        p = p * i
    end
    return p
end

print(recursive_fact(5))  -- 120
print(iterative_fact(5))  -- 120
