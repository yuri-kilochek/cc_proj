local function quicksort(A)
    local function sort(f, l)
        if f < l then
            local p = A[f]
            local i, j = f - 1, l + 1
            while true do
                repeat i = i + 1 until A[i] >= p
                repeat j = j - 1 until A[j] <= p
                if i >= j then break end
                A[i], A[j] = A[j], A[i]
            end

            sort(f, j)
            sort(j + 1, l)
        end
    end

    sort(1, #A)
end

print('Enter number count:')
n = tonumber(scan())
print('Enter numbers:')
A = {}
for i = 1, n do
    A[i] = tonumber(scan())
end

print('Sorted numbers:')
quicksort(A)
for i = 1, #A do
	print(A[i])
end
