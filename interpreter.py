def normalize(chunk, spans):
    def normalize_block(inblock):
        outblock = []
        for instat in inblock:
            outstats = []

            if instat[0] == '$':
                outstats = [
                    ['$', normalize_expr(instat[1])]
                ]

            elif instat[0] == 'do':
                outstats = [
                    ['do', normalize_block(instat[1])],
                ]

            elif instat[0] == 'if':
                outstat = ['if']
                for i in range(len(instat[1:]) // 2):
                    outstat += [normalize_expr(instat[1 + 2*i])]
                    outstat += [normalize_block(instat[1 + 2*i + 1])]
                if len(instat[1:]) % 2 == 1:
                    outstat += [normalize_block(instat[-1])]
                outstats = [outstat]

            elif instat[0] == 'loop':
                outstats = [
                    ['loop', normalize_block(instat[1])],
                ]

            elif instat[0] == 'while':
                outstats = normalize_block([
                    ['loop', [
                        ['if', ['not', instat[1]], [
                            ['break'],
                        ]],
                        *instat[2],
                    ]],
                ])

            elif instat[0] == 'repeat':
                outstats = normalize_block([
                    ['loop', [
                        *instat[1],
                        ['if', instat[2], [
                            ['break'],
                        ]],
                    ]],
                ])

            elif instat[0] == 'for':
                outstats = normalize_block([
                    ['do', [
                        ['local', [
                            ['name', b'(var)'],
                            ['name', b'(limit)'],
                            ['name', b'(step)'],
                        ], [
                            ['call', ['global_name', b'tonumber'], [instat[2]]],
                            ['call', ['global_name', b'tonumber'], [instat[3]]],
                            ['call', ['global_name', b'tonumber'], [instat[4]]],
                        ]],
                        ['if', ['not',
                            ['and',
                                ['name', b'(var)'],
                                ['and',
                                    ['name', b'(limit)'],
                                    ['name', b'(step)'],
                                ],
                            ],
                        ], [
                            ['$', ['call', ['global_name', b'error'], []]],
                        ]],
                        ['=', [
                            ['name', b'(var)'],
                        ], [
                            ['-', ['name', b'(var)'], ['name', b'(step)']],
                        ]],
                        ['loop', [
                            ['=', [
                                ['name', b'(var)'],
                            ], [
                                ['+', ['name', b'(var)'], ['name', b'(step)']],
                            ]],
                            ['if', ['or',
                                ['and',
                                    ['>=',
                                        ['name', b'(step)'],  
                                        ['integer', 0],  
                                    ],
                                    ['>',
                                        ['name', b'(var)'],  
                                        ['name', b'(limit)'],  
                                    ],
                                ],
                                ['and',
                                    ['<',
                                        ['name', b'(step)'],  
                                        ['integer', 0],  
                                    ],
                                    ['<',
                                        ['name', b'(var)'],  
                                        ['name', b'(limit)'],  
                                    ],
                                ],
                            ], [
                                ['break'] ,
                            ]],
                            ['local', [
                                instat[1],
                            ], [
                                ['name', b'(var)'],
                            ]],
                            *instat[5],
                        ]],
                    ]],
                ])

            elif instat[0] == 'for_in':
                outstats = normalize_block([
                    ['do', [
                        ['local', [
                            ['name', b'(f)'],
                            ['name', b'(s)'],
                            ['name', b'(var)'],
                        ], instat[2]],
                        ['loop', [
                            ['local', instat[1], [
                                ['call', ['name', b'(f)'], [
                                    ['name', b'(s)'],
                                    ['name', b'(var)'],
                                ]],
                            ]],
                            ['if', ['==', instat[1][0], ['nil']], [
                                ['break'],
                            ]],
                            ['=', [
                                ['name', b'(var)'],
                            ], [
                                instat[1][0],
                            ]],
                            *instat[3],
                        ]],
                    ]],
                ])

            elif instat[0] == '=':
                outstats = [
                    ['=', list(map(normalize_expr, instat[1])), list(map(normalize_expr, instat[2]))],
                ]

            elif instat[0] == 'local':
                outstats = [
                    ['local', instat[1], list(map(normalize_expr, instat[2]))],
                ]

            elif instat[0] == 'local_function':
                outstats = normalize_block([
                    ['local', [instat[1]], []],
                    ['=', [instat[1]], [
                        ['function', *instat[2:]],
                    ]],
                ])

            elif instat[0] == 'slot_function':
                slot, parameters, body = instat[1:]
                if slot[0] == 'method':
                    slot2 = ['field', slot[1], slot[2]]
                    outspans[id(slot2)] = inspans[id(slot)]
                    slot = slot2
                    parameters = [['name', b'self'], *parameters]
                outstats = normalize_block([
                    ['=', [slot], [
                        ['function', parameters, body],
                    ]],
                ])

            elif instat[0] == 'return':
                outstats = [
                    ['return', list(map(normalize_expr, instat[1]))]
                ]

            else:
                outstats = [instat]

            start, end = spans.get(id(instat), (None, None))
            if len(outstats) == 1:
                spans[id(outstats[0])] = start, end
            elif len(outstats) > 1:
                spans[id(outstats[0])] = start, None
                spans[id(outstats[-1])] = None, end

            outblock += outstats

        return outblock

    def normalize_expr(inexpr):
        if inexpr[0] == '()':
            outexpr = ['()', normalize_expr(inexpr[1])]

        elif inexpr[0] in ('...', 'name', 'global_name', 'nil', 'boolean', 'integer', 'float', 'string'):
            outexpr = inexpr

        elif inexpr[0] == 'table':
            outitems = []
            for initem in inexpr[1]:
                if initem[0] == 'field':
                    name, value = initem[1:]
                    key = normalize_expr(['string', name[1]])
                    spans[id(key)] = spans[id(name)]
                    value = normalize_expr(value)
                    outitem = ['explicit', key, value]
                elif initem == 'slot':
                    outitem = ['explicit', normalize_expr(initem[1]), normalize_expr(initem[2])]
                else:
                    assert initem[0] == 'value'
                    outitem = ['implicit', normalize_expr(initem[1])]
                spans[id(outitem)] = spans[id(initem)]
                outitems += [outitem]
            outexpr = ['table', outitems]

        elif inexpr[0] == 'function':
            outexpr = ['function', inexpr[1], normalize_block(inexpr[2])]

        elif inexpr[0] == 'call':
            outexpr = ['call', normalize_expr(inexpr[1]), list(map(normalize_expr, inexpr[2]))]

        elif inexpr[0] == 'method':
            outexpr = normalize_expr(['call',
                ['function', [['name', b'object']], [
                    ['return', [
                        ['function', ['...'], [
                            ['return', [
                                ['call', ['field', ['name', b'object'], inexpr[2]], [
                                    ['name', b'object'],
                                    ['...'],
                                ]],
                            ]],
                        ]],
                    ]],
                ]],
                [inexpr[1]],
            ])

        elif inexpr[0] == 'field':
            table, name = inexpr[1:]
            key = ['string', name[1]]
            spans[id(key)] = spans[id(name)]
            outexpr = normalize_expr(['slot', table, key])

        elif inexpr[0] == 'slot':
            outexpr = ['slot', normalize_expr(inexpr[1]), normalize_expr(inexpr[2])]

        elif inexpr[0] == 'or':
            outexpr = normalize_expr(['call',
                ['function', [['name', '(left)']], [
                    ['if', ['name', '(left)'], [
                        ['return', [['name', '(left)']]],    
                    ], [
                        ['return', [['()', inexpr[2]]]],    
                    ]],
                ]],
                [inexpr[1]],
            ])

        elif inexpr[0] == 'and':
            outexpr = normalize_expr(['call',
                ['function', [['name', '(left)']], [
                    ['if', ['name', '(left)'], [
                        ['return', [['()', inexpr[2]]]],    
                    ], [
                        ['return', [['name', '(left)']]],    
                    ]],
                ]],
                [inexpr[1]],
            ])

        elif inexpr[0] == '~=':
            outexpr = normalize_expr(['not', ['==', inexpr[1], inexpr[2]]])

        elif inexpr[0] == '>':
            outexpr = normalize_expr(['<', inexpr[2], inexpr[1]])

        elif inexpr[0] == '>=':
            outexpr = normalize_expr(['<=', inexpr[2], inexpr[1]])

        else:
            outexpr = [inexpr[0], *map(normalize_expr, inexpr[1:])]

        spans[id(outexpr)] = spans.get(id(inexpr), (None, None))

        return outexpr

    return normalize_block(chunk), spans

####################

def adjust(count, values):
    if count < len(values):
        return values[:count]
    return [*values, *([None] * (count - len(values)))]

def make_arith_2op(name, primitive_func):
    async def op(args, pos):
        a, b = (await tonumber([args[0]]))[0], (await tonumber([args[1]]))[0]
        if a is not None and b is not None:
            return [primitive_func(a, b)]
        if a.__class__ == Table and a.metatable is not None and (b'__' + name) in a.metatable:
            return await a.metatable[b'__' + name]([a, b])
        if b.__class__ == Table and b.metatable is not None and (b'__' + name) in b.metatable:
            return await b.metatable[b'__' + name]([a, b])
        raise Error('no metamethod __{} found'.format(name.decode()), *pos)
    return op

def make_arith_1op(name, primitive_func):
    async def op(args, pos):
        a = (await tonumber([args[0]]))[0]
        if a is not None:
            return [primitive_func(a)]
        if a.__class__ == Table and a.metatable is not None and (b'__' + name) in a.metatable:
            return await a.metatable[b'__' + name]([a])
        raise Error('no metamethod __{} found'.format(name.decode()), *pos)
    return op

def make_bit_2op(name, primitive_func):
    async def op(args, pos):
        a, b = (await tonumber([args[0]]))[0], (await tonumber([args[1]]))[0]
        if a is not None and b is not None:
            return [primitive_func(round(a), round(b))]
        if a.__class__ == Table and a.metatable is not None and (b'__' + name) in a.metatable:
            return await a.metatable[b'__' + name]([a, b])
        if b.__class__ == Table and b.metatable is not None and (b'__' + name) in b.metatable:
            return await b.metatable[b'__' + name]([a, b])
        raise Error('no metamethod __{} found'.format(name.decode()), *pos)
    return op

def make_bit_1op(name, primitive_func):
    async def op(args, pos):
        a = (await tonumber([args[0]]))[0]
        if a is not None:
            return [primitive_func(round(a))]
        if a.__class__ == Table and a.metatable is not None and (b'__' + name) in a.metatable:
            return await a.metatable['__' + name]([a])
        raise Error('no metamethod __{} found'.format(name.decode()), *pos)
    return op

async def op_concat(args, pos):
    a, b = args
    if a.__class__ in (str, int, float) and b.__class__ in (str, int, float):
        return [str(a) + str(b)]
    if a.__class__ == Table and a.metatable is not None and b'__concat' in a.metatable:
        return await a.metatable[b'__concat']([a, b])
    if b.__class__ == Table and b.metatable is not None and b'__concat' in b.metatable:
        return await b.metatable[b'__concat']([a, b])
    if a.__class__ == Table and b.__class__ == Table:
        return [a.concat(b)]
    raise Error('cannot __concat', *pos)

async def op_len(args, pos):
    (a,) = args
    if a.__class__ == str:
        return [len(a)]
    if a.__class__ == Table:
        if a.metatable is not None and b'__len' in a.metatable:
            return await a.metatable[b'__len']([a])
        else:
            return [max((i for i in a.keys()
                           if i.__class__ == int and all(ii in a for ii in range(1, i))), default=0)]
    raise Error('cannot __len', *pos)

async def op_eq(args, pos):
    a, b = args
    if a.__class__ == Table and b.__class__ == Table:
        if a is b:
            return [True]
        if a.metatable is not None and b'__eq' in a.metatable:
            return await a.metatable[b'__eq']([a, b])
        if b.metatable is not None and b'__eq' in b.metatable:
            return await b.metatable[b'__eq']([a, b])
    else:
        return [a == b]

async def op_lt(args, pos):
    a, b = args
    if a.__class__ == str and b.__class__ == str:
        return [a < b]
    if a.__class__ in (int, float) and b.__class__ in (int, float):
        return [a < b]
    if a.__class__ == Table and a.metatable is not None and b'__lt' in a.metatable:
        return await a.metatable[b'__lt']([a, b])
    if b.__class__ == Table and b.metatable is not None and b'__lt' in b.metatable:
        return await b.metatable[b'__lt']([a, b])
    raise Error('cannot __lt', *pos)

async def op_le(args, pos):
    a, b = args
    if a.__class__ == str and b.__class__ == str:
        return [a <= b]
    if a.__class__ in (int, float) and b.__class__ in (int, float):
        return [a <= b]
    if a.__class__ == Table and a.metatable is not None and b'__le' in a.metatable:
        return await a.metatable[b'__le']([a, b])
    if b.__class__ == Table and b.metatable is not None and b'__le' in b.metatable:
        return await b.metatable[b'__le']([a, b])
    if a.__class__ == Table and a.metatable is not None and b'__lt' in a.metatable:
        return [not (await a.metatable[b'__lt']([b, a]))[0]]
    if b.__class__ == Table and b.metatable is not None and b'__le' in b.metatable:
        return [not (await b.metatable[b'__lt']([b, a]))[0]]
    raise Error('cannot __le', *pos)

async def op_index(args, pos):
    t, k = args
    if k is None:
        raise Error('nil key', *pos)
    if t.__class__ == Table and k in t:
        return [t[k]]
    if t.__class__ == Table and t.metatable is not None and b'__index' in t.metatable:
        idx = t.metatable[b'__index']
        if idx.__class__ == Table:
            return await op_index([idx, k])
        return await idx([t, k])
    raise Error('cannot __index', *pos)

async def op_newindex(args, pos):
    t, k, v = args
    if k is None:
        raise Error('nil key', *pos)
    if t.__class__ == Table:
        if t.metatable is not None and b'__newindex' in t.metatable:
            nidx = t.metatable[b'__newindex']
            if nidx.__class__ == Table:
                await op_newindex([nidx, k, v])
                return
            await nidx([t, k, v])
            return
        t[k] = v
        return
    raise Error('cannot __newindex', *pos)

async def op_call(args, pos):
    f, *args = args
    if callable(f):
        return await f(args)
    if f.__class__ == Table and f.metatable is not None and b'__call' in f.metatable:
        ff = f.metatable[b'__call']
        return await op_call([ff, f, *args])
    raise Error('cannot __call', *pos)

operators = {
    (2, '+'): make_arith_2op(b'add', lambda x, y: x + y),
    (2, '-'): make_arith_2op(b'sub', lambda x, y: x - y),
    (2, '*'): make_arith_2op(b'mul', lambda x, y: x * y),
    (2, '/'): make_arith_2op(b'div', lambda x, y: x / y),
    (2, '%'): make_arith_2op(b'mod', lambda x, y: x % y),
    (2, '^'): make_arith_2op(b'pow', lambda x, y: x ** y),
    (1, '-'): make_arith_1op(b'unm', lambda x: -x),
    (2, '//'): make_arith_2op(b'idiv', lambda x, y: x // y),
    (2, '&'): make_bit_2op(b'band', lambda x, y: x & y),
    (2, '|'): make_bit_2op(b'bor', lambda x, y: x | y),
    (2, '~'): make_bit_2op(b'bxor', lambda x, y: x ^ y),
    (1, '~'): make_bit_1op(b'bnot', lambda x: ~x),
    (2, '<<'): make_bit_2op(b'shl', lambda x, y: x << y),
    (2, '>>'): make_bit_2op(b'shr', lambda x, y: x >> y),
    (2, '..'): op_concat,
    (1, '#'): op_len,
    (2, '=='): op_eq,
    (2, '<'): op_lt,
    (2, '<='): op_le,
}

async def tonumber(args):
    if isinstance(args[0], (int, float)):
        return [args[0]]
    if isinstance(args[0], str):
        if args[0].startswith('0x') or args[0].startswith('0X'):
            try:
                return [int(args[0], 16)]
            except ValueError:
                pass
            try:
                return [float.fromhex(args[0])]
            except ValueError:
                pass
        else:
            try:
                return [int(args[0])]
            except ValueError:
                pass
            try:
                return [float(args[0])]
            except ValueError:
                pass
    return [None]

async def _print(args):
    def normalize(arg):
        if arg is None:
            return 'nil'
        if arg.__class__ == bytes:
            return arg.decode()

        return arg

    print(*map(normalize, args))
    return []

async def scan(args):
    return [input()]

async def error(message):
    raise Error(message.encode())

class Table(dict):
    def __init__(self, *args, **kwargs):
        self.metatable = None

    def concat(self, other):
        result = Table()
        i = 0


class Context:
    def __init__(self, parent=None):
        self.__parent = parent
        self.__values = {}

        self.__values[b'tonumber'] = tonumber
        self.__values[b'scan'] = scan
        self.__values[b'print'] = _print
        self.__values[b'error'] = error

    def declare(self, name, value):
        self.__values[name] = value

    def assign(self, name, value):
        ctx = self
        while True:
            if name in ctx.__values or ctx.__parent is None:
                ctx.__values[name] = value
                return
            ctx = ctx.__parent

    def get(self, name):
        ctx = self
        while True:
            if name in ctx.__values or ctx.__parent is None:
                return ctx.__values.get(name, None)
            ctx = ctx.__parent

    def push(self):
        return Context(self)


class Error(Exception):
    @property
    def message(self):
        return self.args[0]

    @property
    def line(self):
        return self.args[1]

    @property
    def column(self):
        return self.args[2]

class Goto(Exception):
    @property
    def label(self):
        return self.args[0]

    @property
    def line(self):
        return self.args[1]

    @property
    def column(self):
        return self.args[2]

class Break(Exception):
    @property
    def line(self):
        return self.args[0]

    @property
    def column(self):
        return self.args[1]

class Return(Exception):
    @property
    def values(self):
        return self.args[0]


def compile(chunk, spans, global_ctx):
    async def evaluate_block(stats, ctx):
        ctx = ctx.push()

        async def evaluate_expr(e):
            if e[0] == '()':
                return adjust(1, await evaluate_expr(e[1]))
                    
            if e[0] == 'name':
                return [ctx.get(e[1])]

            if e[0] == 'global_name':
                return [global_ctx.get(e[1])]

            if e[0] == 'nil':
                return [None]

            if e[0] == 'boolean':
                return [e[1]]

            if e[0] == 'integer':
                return [e[1]]

            if e[0] == 'float':
                return [e[1]]

            if e[0] == 'string':
                return [e[1]]

            if e[0] == 'table':
                table = Table()
                implicit_key = 1
                for i, item in enumerate(e[1]):
                    if item[0] == 'explicit':
                        key = adjust(1, await evaluate_expr(item[1]))[0]
                        value = adjust(1, await evaluate_expr(item[2]))[0]
                        await op_newindex([table, key, value], spans.get(id(item), [(None, None)])[0])
                        continue
                    if item[0] == 'implicit':
                        values = await evaluate_expr(item[1])
                        if i < len(e[1]) - 1:
                            values = adjust(1, values)
                        for value in values:
                            key = implicit_key
                            implicit_key += 1
                            await op_newindex([table, key, value], spans.get(id(item), [(None, None)])[0])
                        continue
                    assert False
                return [table]

            if e[0] == 'slot':
                table = adjust(1, await evaluate_expr(e[1]))[0]
                key = adjust(1, await evaluate_expr(e[2]))[0]
                return await op_index([table, key], spans.get(id(e), [(None, None)])[0])

            if e[0] == 'function':
                async def function(arguments):
                    local_ctx = ctx.push()

                    parameters, body = e[1:]
                    if len(parameters) > 0 and parameters[-1][0] == '...':
                        parameters = parameters[:-1]
                        if len(arguments) > len(parameters):
                            arguments, excess = arguments[:len(parameters)], arguments[len(parameters):]
                        else:
                            arguments, excess = adjust(len(parameters), arguments), []
                    else:
                        arguments, excess = adjust(len(parameters), arguments), None

                    for name, value in zip(parameters, arguments):
                        local_ctx.declare(name[1], value)
                    local_ctx.declare('...', excess)

                    try:
                        await evaluate_block(body, local_ctx)
                    except Goto as g:
                        raise Error('no visible label ::{}:: for goto'.format(g.label), g.line, g.column)
                    except Break as b:
                        raise Error('break outside loop', b.line, b.column)
                    except Return as r:
                        return r.values

                    return []

                return [function]

            if e[0] == 'call':
                function = adjust(1, await evaluate_expr(e[1]))[0]
                arguments = []
                if e[2]:
                    for argument in e[2][:-1]:
                        arguments += adjust(1, await evaluate_expr(argument))
                    arguments += await evaluate_expr(e[2][-1])
                return await op_call([function, *arguments], spans.get(id(e), [(None, None)])[0])

            if e[0] == '...':
                excess = ctx.get('...')
                if excess is None:
                    raise Error('`...` outside vararg function', *spans[id(e)][0])
                return excess

            if e[0] == 'not':
                value = adjust(1, await evaluate_expr(e[1]))[0]
                return [value is None or value.__class__ == bool and not value]

            operands = []
            for operand in e[1:]:
                operands += adjust(1, await evaluate_expr(operand))
            return await operators[len(operands), e[0]](operands, spans.get(id(e), [(None, None)])[0])


        i = 0
        while i < len(stats):
            s = stats[i]
            try:
                if s[0] == '$':
                    await evaluate_expr(s[1])
                    continue

                if s[0] == 'do':
                    await evaluate_block(s[1], ctx)
                    continue

                if s[0] == 'if':
                    async def f():
                        for j in range(len(s[1:]) // 2):
                            condition = adjust(1, await evaluate_expr(s[1:][2 * j]))[0]
                            if not (condition is None or condition.__class__ == bool and not condition):
                                await evaluate_block(s[1:][2 * j + 1], ctx)
                                return
                        if len(s[1:]) % 2 == 1:
                            await evaluate_block(s[-1], ctx)
                    await f()
                    continue

                if s[0] == 'loop':
                    try:
                        while True:
                            await evaluate_block(s[1], ctx)
                    except Break:
                        pass
                    continue

                if s[0] == 'break':
                    raise Break(*(spans[id(s)][0] or ()))

                if s[0] == 'local':
                    values = []
                    if s[2]:
                        for e in s[2][:-1]:
                            values += adjust(1, await evaluate_expr(e))
                        values += await evaluate_expr(s[2][-1])
                    values = adjust(len(s[1]), values)
                    for name, value in zip(s[1], values):
                        ctx.declare(name[1], value)
                    continue

                if s[0] == '=':
                    values = []
                    for e in s[2][:-1]:
                        values += adjust(1, await evaluate_expr(e))
                    values += await evaluate_expr(s[2][-1])
                    values = adjust(len(s[1]), values)
                    for assignable, value in zip(s[1], values):
                        if assignable[0] == 'name':
                            name = assignable[1]
                            ctx.assign(name, value)
                            continue
                        if assignable[0] == 'global_name':
                            global_name = assignable[1]
                            global_ctx.assign(global_name, value)
                            continue
                        if assignable[0] == 'slot':
                            table = adjust(1, await evaluate_expr(assignable[1]))[0]
                            key = adjust(1, await evaluate_expr(assignable[2]))[0]
                            await op_newindex([table, key, value], spans.get(id(s), [(None, None)])[0])
                            continue
                        assert False
                    continue

                if s[0] == 'label':
                    continue

                if s[0] == 'goto':
                    raise Goto(s[1][1], *spans[id(s)][0])

                if s[0] == 'return':
                    values = []
                    if s[1]:
                        for e in s[1][:-1]:
                            values += adjust(1, await evaluate_expr(e))
                        values += await evaluate_expr(s[1][-1])
                    raise Return(values)

            except Goto as g:
                first_violated_name = None
                for j, s in enumerate(statements):
                    if s[0] == 'local' and j > i and first_violated_name is None:
                        first_violated_name = s[1][0]
                    elif s[0] == 'label' and s[1][1] == g.label:
                        if first_violated_name is not None:
                            raise Error('goto ::{}:: enters scope of `{}` at {}:{}'
                                .format(g.label, first_violated_name[1], *spans[id(first_violated_name)][0]),
                                g.line, g.column)
                        i = j
                        break
                else:
                    raise

            finally:
                i += 1

    async def function(arguments=()):
        local_ctx = global_ctx.push()

        local_ctx.declare('...', list(arguments))

        try:
            await evaluate_block(chunk, local_ctx)
        except Goto as g:
            raise Error('no visible label ::{}:: for goto'.format(g.label), g.line, g.column)
        except Break as b:
            raise Error('break outside loop', b.line, b.column)
        except Return as r:
            return r.values

        return []

    return function


if __name__ == '__main__':
    from pprint import pprint

    import sys
    with open(sys.argv[1], 'rb') as source_file:
        source = source_file.read()

    import tokenizer
    try:
        tokens, spans = tokenizer.tokenize(source)
    except tokenizer.TokenizeError as e:
        print('{}:{}:{} Tokenize error.'.format(sys.argv[1], e.line or '?', e.column or '?'))
        exit(1)

    import parser
    try:
        chunk, spans = parser.parse(tokens, spans)
    except parser.ParseError as e:
        print('{}:{}:{} Parse error: {}.'.format(sys.argv[1], e.line or '?', e.column or '?', e.message))
        exit(2)

    chunk, spans = normalize(chunk, spans)
    chunk = compile(chunk, spans, Context())
    try:
        chunk().send(None)
    except Error as e:
        print('{}:{}:{} Runtime error: {}.'.format(sys.argv[1], e.line or '?', e.column or '?', e.message))
        exit(3)
    except StopIteration:
        pass

