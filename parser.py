class ParseError(ValueError):
    @property
    def message(self):
        return self.args[0]

    @property
    def line(self):
        return self.args[1]

    @property
    def column(self):
        return self.args[2]

def parse(tokens, spans):
    token_i = 0

    def eat(label):
        nonlocal token_i
        if token_i < len(tokens) and tokens[token_i][0] == label:
            token = tokens[token_i]
            token_i += 1
            return token
        return None

    def ask(description, position=None):
        if position is None:
            if token_i > 0:
                position = ends[id(tokens[token_i - 1])] 
            else:
                position = starts[id(tokens[0])] 
        raise ParseError('expected {}'.format(description), *position)

    starts, ends = {}, {}
    for k, span in spans.items():
        starts[k], ends[k] = span
       
    #######################

    def parse_block():
        block = []

        while True:
            statement = []
            starts[id(statement)] = starts[id(tokens[token_i])]

            if eat(';'):
                del starts[id(statement)]
                continue

            elif eat('do'):
                statement += ['do']
                statement += [parse_block()]
                eat('end') or ask('`end`')

            elif eat('if'):
                statement += ['if']
                statement += [parse_expression() or ask('condition')]
                eat('then')
                statement += [parse_block()]
                while eat('elseif'):
                    statement += [parse_expression() or ask('condition')]
                    eat('then')
                    statement += [parse_block()]
                if eat('else'):
                    statement += [parse_block()]
                eat('end') or ask('statement or `end`')

            elif eat('while'):
                statement += ['while']
                statement += [parse_expression() or ask('condition')]
                eat('do')
                statement += [parse_block()]
                eat('end') or ask('`end`')

            elif eat('repeat'):
                statement += ['repeat']
                statement += [parse_block()]
                eat('until') or ask('`until`')
                statement += [parse_expression() or ask('condition')]

            elif eat('for'):
                variable_name = eat('name') or ask('variable name')
                if eat('='):
                    statement += ['for']
                    statement += [variable_name]
                    statement += [parse_expression() or ask('range start')]
                    eat(',') or ask('`,`')
                    statement += [parse_expression() or ask('range end')]
                    if eat(','):
                        statement += [parse_expression() or ask('range step')]
                    else:
                        statement += [['integer', 1]]
                    eat('do')
                    statement += [parse_block()]
                    eat('end') or ask('`end`')
                else:
                    statement += ['for_in']
                    variable_names = [variable_name]
                    while eat(','):
                        variable_names += [eat('name') or ask('variable name')]
                    statement += [variable_names]
                    eat('in') or ask('`,` or `in`' if len(variable_names) > 1 else '`,`, `=` or `in`')
                    initializers = [parse_expression() or ask('iterator function, state and initial value')]
                    if eat(','):
                        initializers += [parse_expression() or ask('iterator state and initial value')]
                    if eat(','):
                        initializers += [parse_expression() or ask('initial iterator value')]
                    while eat(','):
                        initializers += [parse_expression() or ask('expression')]
                    statement += [initializers]
                    eat('do')
                    statement += [parse_block()]
                    eat('end') or ask('`end`')

            elif eat('break'):
                statement += ['break']

            elif eat('::'):
                statement += ['label']
                statement += [eat('name') or ask('label name')]
                eat('::') or ask('`::`')

            elif eat('goto'):
                statement += ['goto']
                statement += [eat('name') or ask('label name')]

            elif eat('local'):
                if eat('function'):
                    statement += ['local_function']
                    statement += [eat('name') or ask('function name')]
                    statement += [*parse_function_tail()]
                else:
                    statement += ['local']
                    variable_names = [eat('name') or ask('variable name')]
                    while eat(','):
                        variable_names += [eat('name') or ask('variable name')]
                    statement += [variable_names]
                    initializers = []
                    if eat('='):
                        initializers += [parse_expression() or ask('initializer expression')]
                        while eat(','):
                            initializers += [parse_expression() or ask('initializer expression')]
                    statement += [initializers]

            elif eat('function'):
                statement += ['slot_function']
                slot = eat('name') or ask('function name')
                while True:
                    if eat(':'):
                        slot2 = ['method', slot, eat('name') or ask('method name')]
                        starts[id(slot2)] = starts[id(slot)]
                        ends[id(slot2)] = ends[id(slot)]
                        slot = slot2
                        break
                    if eat('.'):
                        slot2 = ['field', slot, eat('name') or ask('field name')]
                        starts[id(slot2)] = starts[id(slot)]
                        ends[id(slot2)] = ends[id(slot)]
                        slot = slot2
                        continue
                    break
                statement += [slot]
                statement += [*parse_function_tail()]

            elif eat('return'):
                statement += ['return']
                result = parse_expression()
                if result:
                    results = [result]
                    while eat(','):
                        results += [parse_expression() or ask('result expression')]
                else:
                    results = []
                statement += [results]

            else:
                expression = parse_expression()
                if not expression:
                    del starts[id(statement)]
                    break
                expressions = [expression]
                while eat(','):
                    expressions += [parse_expression() or ask('assignable expression')]
                if len(expressions) > 1:
                    eat('=') or ask('`=`')
                    expression = None
                    slots = expressions
                elif eat('='):
                    expression = None
                    slots = expressions
                else:
                    expression = expressions[0]
                if expression:
                    statement += ['$']
                    statement += [expression]
                else:
                    statement += ['=']
                    for slot in slots:
                        if slot[0] not in {'slot', 'field', 'name'}:
                            ask('assignable expression', starts[id(slot)])
                    statement += [slots]
                    initializers = [parse_expression() or ask('initializer expression')]
                    while eat(','):
                        initializers += [parse_expression() or ask('initializer expression')]
                    statement += [initializers]

            ends[id(statement)] = ends[id(tokens[token_i - 1])]
            block += [statement]

        if len(block) == 0:
            if token_i == 0:
                starts[id(block)] = (1, 1)
            else:
                starts[id(block)] = ends[id(tokens[token_i - 1])]
            ends[id(block)] = starts[id(tokens[token_i])]
        else:
            starts[id(block)] = starts[id(block[0])]
            ends[id(block)] = ends[id(block[-1])]

        return block

    def parse_expression():
        return parse_logical_or()

    def parse_right_infix(parse_component, operators):
        node = parse_component()
        if node is None:
            return None

        for operator in operators:
            if eat(operator):
                left, right = node, parse_right_infix(parse_component, operators) or ask('right operand of `{}`'.format(operator))

                node = [operator, left, right]

                starts[id(node)] = starts[id(left)]
                ends[id(node)] = ends[id(right)]

                break

        return node

    def parse_left_infix(parse_component, operators):
        node = parse_component()
        if node is None:
            return None

        while True:
            for operator in operators:
                if eat(operator):
                    left, right = node, parse_component() or ask('right operand of `{}`'.format(operator))

                    node = [operator, left, right]

                    starts[id(node)] = starts[id(left)]
                    ends[id(node)] = ends[id(right)]

                    break
            else:
                break

        return node

    def parse_logical_or():
        return parse_right_infix(parse_logical_and, {'or'})

    def parse_logical_and():
        return parse_right_infix(parse_relation, {'and'})

    def parse_relation():
        return parse_left_infix(parse_bitwise_or, {'==', '~=', '<', '<=', '>', '>='})

    def parse_bitwise_or():
        return parse_left_infix(parse_bitwise_xor, {'|'})

    def parse_bitwise_xor():
        return parse_left_infix(parse_bitwise_and, {'~'})

    def parse_bitwise_and():
        return parse_left_infix(parse_bitshift, {'&'})

    def parse_bitshift():
        return parse_left_infix(parse_concatenation, {'<<', '>>'})

    def parse_concatenation():
        return parse_right_infix(parse_additive_arithmetic, {'..'})

    def parse_additive_arithmetic():
        return parse_left_infix(parse_multiplicative_arithmetic, {'+', '-'})

    def parse_multiplicative_arithmetic():
        return parse_left_infix(parse_prefix, {'*', '/', '//', '%'})

    def parse_prefix():
        for operator in {'not', '#', '-', '~'}:
            if eat(operator):
                node = [operator]
                starts[id(node)] = starts[id(tokens[token_i - 1])]
                node += [parse_prefix() or ask('operand of `{}`'.format(operator))]
                ends[id(node)] = ends[id(tokens[token_i - 1])]
                return node
        return parse_arithmetic_power()

    def parse_arithmetic_power():
        node = parse_postfix()
        if node is None:
            return None

        if eat('^'):
            left, right = node, parse_prefix() or ask('right operand of `^`')

            node = ['^', left, right]

            starts[id(node)] = starts[id(left)]
            ends[id(node)] = ends[id(right)]

        return node

    def parse_postfix():
        thunk = parse_primary()
        if thunk is None:
            return None

        while True:
            postfix = None
            try:
                ee = None

                if eat('.'):
                    postfix = ['field', thunk, eat('name') or ask('field name')]
                    continue

                if eat(':'):
                    postfix = ['method', thunk, eat('name') or ask('method name')]
                    continue

                if eat('['):
                    postfix = ['slot', thunk, parse_expression() or ask('slot key')]
                    eat(']') or ask('`]`')
                    continue

                if eat('('):
                    arguments = []
                    argument = parse_expression()
                    if argument:
                        arguments += [argument]
                        while eat(','):
                            arguments += [parse_expression() or ask('argument expression')]
                    postfix = ['call', thunk, arguments]
                    eat(')') or ask('`)`')
                    continue

                string = eat('string')
                if string:
                    postfix = ['call', thunk, [string]]
                    continue

                table = parse_table()
                if table:
                    postfix = ['call', thunk, [table]]
                    continue

            except Exception as e:
                ee = e
            finally:
                if ee:
                    raise ee

                if postfix is None:
                    break

                starts[id(postfix)] = starts[id(thunk)]
                ends[id(postfix)] = ends[id(tokens[token_i - 1])]
                thunk = postfix

        return thunk

    def parse_primary():
        if eat('('):
            parenthesized = ['()']
            starts[id(parenthesized)] = starts[id(tokens[token_i - 1])]
            parenthesized += [parse_expression() or ask('expression')]
            eat(')') or ask('`)`')
            ends[id(parenthesized)] = ends[id(tokens[token_i - 1])]
            return parenthesized

        token = eat('false') or eat('true')
        if token:
            if token[0] == 'false':
                node = ['boolean', False]
            elif token[0] == 'true':
                node = ['boolean', True]
            else:
                assert False

            starts[id(node)] = starts[id(token)]
            ends[id(node)] = ends[id(token)]

            return node

        token = eat('name') or eat('...') or eat('nil') or eat('integer') or eat('float') or eat('string')
        if token:
            return token

        table = parse_table()
        if table:
            return table

        function = parse_function()
        if function:
            return function

        return None

    def parse_table():
        if eat('{'):
            table = ['table']
            starts[id(table)] = starts[id(tokens[token_i - 1])]
            items = []
            item = parse_table_item()
            if item:
                items += [item]
                while eat(',') or eat(';'):
                    items += [parse_table_item() or eat('table item')]
            table += [items]
            items and eat(',') or eat(';')
            eat('}') or ask('`}`')
            ends[id(table)] = ends[id(tokens[token_i - 1])]
            return table
        return None

    def parse_table_item():
        if eat('['):
            slot = ['slot']
            starts[id(slot)] = starts[id(tokens[token_i - 1])]
            slot += [parse_expression()]
            eat(']') or ask('`]`')
            eat('=') or ask('`=`')
            slot += [parse_expression()]
            ends[id(slot)] = ends[id(tokens[token_i - 1])]
            return slot
        name_or_value = parse_expression()
        if name_or_value is None:
            return None
        if eat('='):
            name_or_value[0] == 'name' or ask('field name', starts[id(name_or_value)])
            field = ['field']
            starts[id(field)] = starts[id(name_or_value)]
            field += [name_or_value]
            field += [parse_expression() or ask('value expression')]
            ends[id(field)] = ends[id(tokens[token_i - 1])]
            return field
        value = ['value']
        starts[id(value)] = starts[id(name_or_value)]
        value += [name_or_value]
        ends[id(value)] = ends[id(name_or_value)]
        return value

    def parse_function():
        if eat('function'):
            node = ['function']
            starts[id(node)] = starts[id(tokens[token_i - 1])]
            node += [*parse_function_tail()]
            ends[id(node)] = ends[id(tokens[token_i - 1])]
            return node
        return None

    def parse_function_tail():
        eat('(') or ask('`(`')
        parameters = []
        parameter = eat('name') or eat('...')
        if parameter:
            parameters += [parameter]
            while eat(','):
                parameters += [eat('name') or eat('...') or ask('parameter name or `...`')]
        for parameter in parameters[:-1]:
            if parameter[0] == '...':
                ask('`)`', ends[id(parameter)])
        eat(')') or ask('`)`')
        body = parse_block()
        eat('end') or ask('`end`')
        return parameters, body

    chunk = parse_block()
    eat(None) or ask('statement or end of chunk')

    spans = {}
    for k in set(starts) | set(ends):
        spans[k] = starts.get(k), ends.get(k)

    return chunk, spans

def parse_file_at(path):
    from tokenizer import tokenize_file_at

    tokens, spans = tokenize_file_at(path)
    return parse(tokens, spans)

