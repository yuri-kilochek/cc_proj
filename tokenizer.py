import re as _re

_keywords = {
    b'and',    
    b'break',    
    b'do',    
    b'else',    
    b'elseif',    
    b'end',    
    b'false',    
    b'for',    
    b'function',    
    b'goto',    
    b'if',    
    b'in',    
    b'local',    
    b'nil',    
    b'not',    
    b'or',    
    b'repeat',    
    b'return',    
    b'then',    
    b'true',    
    b'until',    
    b'while',    
}

_string_escapes = {_re.compile(p, _re.VERBOSE | _re.DOTALL): r for p, r in {
    rb'\\a': lambda m: b'\a',
    rb'\\b': lambda m: b'\b',
    rb'\\f': lambda m: b'\f',
    rb'\\n': lambda m: b'\n',
    rb'\\r': lambda m: b'\r',
    rb'\\t': lambda m: b'\t',
    rb'\\v': lambda m: b'\v',
    rb'\\\\': lambda m: b'\\',
    rb'\\"': lambda m: b'"',
    rb"\\'": lambda m: b"'",
    rb'\\\n': lambda m: b'\n',
    rb'\\z[ \t\v\f\n]*': lambda m: b'',
    rb'\\x([0-9A-Za-z]{2})': lambda m: bytes([int(m.string[m.start(1):m.end(1)].decode(), 16)]),
    rb'\\[0-9]{1,3}': lambda m: bytes([int(m.string[m.start():m.end()].decode())]),
    rb'\\u\{([0-9A-Za-z]+)\}': lambda m: chr(int(m.string[m.start(1):m.end(1)].decode(), 16)).encode(),
}.items()}

_tokens = {t: _re.compile(p, _re.VERBOSE | _re.DOTALL) for t, p in {
    'whitespace': rb'''
        [ \t\v\f\n]+
    ''',

    'comment_line': rb'''
        \-\- (?! \[ \=* \[ )
        .*?
        \n
    ''',
    'comment_block': rb'''
        \-\-
        \[ (?P<d> \=* ) \[
        .*?
        \] (?P=d)       \]
    ''',

    '+': rb'''\+''',
    '-': rb'''\-(?!\-)''',
    '*': rb'''\*''',
    '/': rb'''\/(?!\/)''',
    '%': rb'''\%''',
    '^': rb'''\^''',
    '#': rb'''\#''',
    '&': rb'''\&''',
    '~': rb'''\~(?!\=)''',
    '|': rb'''\|''',
    '<<': rb'''\<\<''',
    '>>': rb'''\>\>''',
    '//': rb'''\/\/''',
    '==': rb'''\=\=''',
    '~=': rb'''\~\=''',
    '<=': rb'''\<\=''',
    '>=': rb'''\>\=''',
    '<': rb'''\<(?!\<)(?!\=)''',
    '>': rb'''\>(?!\>)(?!\=)''',
    '=': rb'''\=(?!\=)''',
    '(': rb'''\(''',
    ')': rb'''\)''',
    '{': rb'''\{''',
    '}': rb'''\}''',
    '[': rb'''\[(?!\=)(?!\[)''',
    ']': rb'''\]''',
    '::': rb'''\:\:''',
    ';': rb'''\;''',
    ':': rb'''\:(?!\:)''',
    ',': rb'''\,''',
    '.': rb'''\.(?!\.)''',
    '..': rb'''\.\.(?!\.)''',
    '...': rb'''\.\.\.''',

    'symbol': rb'''
        [_A-Za-z]
        [_A-Za-z0-9]*
    ''',

    'number_10': rb'''
        [0-9]+
        (?:\.[0-9]+)?
        (?:[Ee][-+]?[0-9]+)?
        (?![_A-Za-z0-9])
    ''',
    'number_16': rb'''
        0[Xx]
        [0-9A-Fa-f]+
        (?:\.[0-9A-Fa-f]+)?
        (?:[Pp][-+]?[0-9]+)?
        (?![_A-Za-z0-9])
    ''',

    'string': rb'''
        (?P<q> ['"] )
        (?P<raw_value> (?: ''' + b' | \n'.join(re.pattern for re in _string_escapes) + rb''' |
                           (?! \\ | (?P=q) ) . )*? )
        (?P=q)
    ''',
    'string_verbatim': rb'''
        \[ (?P<d> \=* ) \[ \n?
        (?P<value> .*? )
        \] (?P=d)       \]
    ''',
}.items()}

class TokenizeError(ValueError):
    @property
    def line(self):
        return self.args[0]

    @property
    def column(self):
        return self.args[1]

def tokenize(text):
    text = _re.sub(b'\r\n|\r|\n\r', b'\n', text)

    line = 1
    column = 1
    lines = [line]
    columns = [column]
    for byte in text:
        if byte == b'\n'[0]:
            line += 1
            column = 1
        else:
            column += 1
        lines.append(line)
        columns.append(column)

    raw_tokens = []
    p = 0
    while p < len(text):
        match = None
        for i_label, i_regexp in _tokens.items():
            i_match = i_regexp.match(text, p)
            if i_match:
                assert match is None
                label, match = i_label, i_match
        if match is None:
            raise TokenizeError(lines[p], columns[p])
        raw_tokens.append((label, match))
        p = match.end()

    tokens = []
    spans = {}
    def emit(token, span):
        start, end = span
        tokens.append(token)
        spans[id(token)] = (lines[start], columns[start]), (lines[end], columns[end])
    for label, match in raw_tokens:
        if label in ('whitespace', 'comment_line', 'comment_block'):
            continue

        if label == 'symbol':
            symbol = match.string[match.start():match.end()]
            if symbol in _keywords:
                emit([symbol.decode()], match.span())
                continue
            emit(['name', symbol], match.span())
            continue

        if label == 'number_10':
            value = match.string[match.start():match.end()].decode()
            if '.' in value or 'e' in value or 'E' in value:
                emit(['float', float(value)], match.span())
            else:
                emit(['integer', int(value)], match.span())
            continue

        if label == 'number_16':
            value = match.string[match.start():match.end()].decode()
            if '.' in value or 'p' in value or 'P' in value:
                emit(['float', float.fromhex(value)], match.span())
            else:
                emit(['integer', int(value, 16)], match.span())
            continue

        if label == 'string':
            value = match.string[match.start('raw_value'):match.end('raw_value')]
            for regexp, replacer in _string_escapes.items():
                try:
                    value = regexp.sub(replacer, value)
                except ValueError:
                    raise TokenizeError(lines[match.start()], columns[match.end()])
            emit(['string', value], match.span())
            continue

        if label == 'string_verbatim':
            value = match.string[match.start('value'):match.end('value')]
            emit(['string', value], match.span())
            continue

        emit([label], match.span())

    tokens.append([None])
    spans[id(tokens[-1])] = (lines[-1], columns[-1]), None

    return tokens, spans

def tokenize_file_at(path):
    with open(path, 'rb') as file:
        text = file.read()
    return tokenize(text)

